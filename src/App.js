import React, { useState, useEffect } from "react";
import "./App.css";
import Form from "./component/form";
import List from "./component/list";
import { create, findAll, update, remove } from "./data-access";

function App() {
  const productData = {
    id: "",
    name: "",
    description: "",
    quantity: 0,
    warranty: "",
    price: 0,
    status: "",
  };

  const [editing, setEditing] = useState(false);
  const [currentProduct, setCurrentProduct] = useState(productData);
  const [products, setProducts] = useState([]);

  useEffect(() => findAll().then((data) => setProducts(data)), []);

  const addProduct = (product) => {
    create(product)
      .then(() => findAll())
      .then((data) => setProducts(data));
    setCurrentProduct(productData);
  };

  const deleteProduct = (id) => {
    remove(id)
      .then(() => findAll())
      .then((data) => setProducts(data));
  };

  const updateProduct = (id, data) => {
    update(id, data)
      .then(() => findAll())
      .then((data) => setProducts(data));
    setEditing(false);
    setCurrentProduct(productData);
  };

  return (
    <div className="app">
      <div className="app-header">PANACASH PRODUCT CRUD</div>
      <Form
        currentProduct={currentProduct}
        addProduct={addProduct}
        updateProduct={updateProduct}
        editing={editing}
        setCurrentProduct={setCurrentProduct}
      />
      <List
        products={products}
        deleteProduct={deleteProduct}
        setEditing={setEditing}
        setCurrentProduct={setCurrentProduct}
      />
      <div className="app-footer"></div>
    </div>
  );
}

export default App;
