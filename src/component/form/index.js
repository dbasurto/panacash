import React, { useState } from "react";
import "./index.css";

function Form({ currentProduct, setCurrentProduct, addProduct, updateProduct, editing }) {

  return (
    <div className="product-form">
      {editing ? <p>Edit product</p> : <p>New product</p>}
      <div className="field">
        <label>Name</label>
        <input
          value={currentProduct.name}
          onChange={(e) => setCurrentProduct({ ...currentProduct, name: e.target.value })}
        />
      </div>
      <div className="field">
        <label>Description</label>
        <input
          value={currentProduct.description}
          onChange={(e) =>
            setCurrentProduct({ ...currentProduct, description: e.target.value })
          }
        />
      </div>
      <div className="field">
        <label>Quantity</label>
        <input
          value={currentProduct.quantity}
          onChange={(e) => setCurrentProduct({ ...currentProduct, quantity: e.target.value })}
        />
      </div>
      <div className="field">
        <label>Warranty</label>
        <input
          value={currentProduct.warranty}
          onChange={(e) => setCurrentProduct({ ...currentProduct, warranty: e.target.value })}
        />
      </div>
      <div className="field">
        <label>Price</label>
        <input
          value={currentProduct.price}
          onChange={(e) => setCurrentProduct({ ...currentProduct, price: e.target.value })}
        />
      </div>
      <div className="field">
        <label>Status</label>
        <input
          value={currentProduct.status}
          onChange={(e) => setCurrentProduct({ ...currentProduct, status: e.target.value })}
        />
      </div>
      <div className="button">
        <button
          className="btn"
          onClick={() => {
            if (editing) {
              updateProduct(currentProduct.id, currentProduct);
            } else {
              addProduct(currentProduct);
            }
          }}
        >
          <i className="fa fa-plus"></i>{" "}
          {editing ? "Edit product" : "New product"}
        </button>
      </div>
    </div>
  );
}

export default Form;
